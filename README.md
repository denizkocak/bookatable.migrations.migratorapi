# RESTful Web API Template

RESTful web API template with example resource, validation, pagination, error handling and API documentation out of the box.

## Running

The application requires VS2017 with dotnet core tooling installed. [Docker for Windows](https://docs.docker.com/docker-for-windows/) is necessary for building/running in Docker.

### Docker

The project is setup to build and run with Docker. To do so locally, install [Docker for Windows](https://docs.docker.com/docker-for-windows/), set `docker-compose` as the startup project within Visual Studio, and run the app. Debugging the app inside a container is a simple as hitting F5. This has the advantage of perfectly simulating the production environment locally. The downside of this is that it is a little slower to run, so for fast local devleopment you may wan to to set the web API as the startup project, and run the app on your machine natively (with or without IIS Express).

When building in Teamcity, `docker-compose.ci.build.yml` is used to build and run tests. This means that no dependencies (other than Docker) need to be installed on build agents. This is compatible with any CI solution that supports Docker. If you would like to test this build locally, you may need to set the `NUGET_CONFIG_PATH` variable to point to your local `NuGet.Config`.

## API Template

The project template is opinionated, and comes with a number of features to make creating basic REST API as easy as possible. It largely based on [this article](http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api) on pragmatic RESTful design.

### REST

The following verbs are used to manage resources:

| Action | VERB | URL | Request Payload | Response Payload | Success Status Code | Headers |
| ------ | ---- | --- | --------------- | ---------------- | ------------------- | ------- |
| Create resource | POST | /foos | foo JSON | foo JSON | 201 created | Location |
| Query resources | GET | /foos?x=a&y=b | N/A | foo JSON array | 200 OK
| Get resource by ID | GET | /foos/{id} | N/A | foo JSON | 200 OK |
| Replace resouce | PUT | /foos/{id} | foo JSON | foo JSON | 200 OK |
| Partially update resouce | PATCH | /foos/{id} | foo [JSON patch](http://jsonpatch.com/) | foo JSON | 200 OK |
| Delete resouce | DELETE | /foos/{id} | N/A | N/A | 200 OK |

Request and response bodies are simple JSON documents representing resources. No request/response envelopes are necessary. The usual response codes are used when resouces are not found, or when requests are invalid (404 & 400).

### Error Handling

When the API doesn't respond with a 2XX response code, the error payload is consistent in order to simplify client code. The error model is as follows and is used for unhandled exceptions, bad requests e.t.c. It must always contain a friendly description in the message field.

```json
{
  "code": "this.code.is.optional",
  "message": "Mandatory friendly description of the error.",
  "details": [
    "Error detail 1.",
    "This is an array to support scenarios where multiple errors have occurred (e.g. validation)."
  ]
}
```

Note that when the application is running locally in development mode, unhandled exception detail will appear in the error detail, otherwise a generic message is used for 500s.

Error handling is achieved using [`ErrorHandlingMiddleware`](src/Bookatable.Migrations.MantleEventsMigrationApi/Middleware/ErrorHandlingMiddleware.cs) rather than an [`ExceptionFilter`](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters#exception-filters) which only handles exceptions in actions.

```csharp
app.UseMiddleware<ErrorHandlingMiddleware>();
app.UseMvc();
```

The middleware must be the first middleware registered in order to ensure all exceptions are caught. It can be extended to respond differently depending on which exception it is handling (this is already used to return 400s when validation exceptions are caught).

### Validation

Validation is done on 2 levels; basic model validation using [annotations](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/validation), and custom validation rules that may depend on business logic. If validation fails a 400 response will be returned with a [stardardised error response](#error-handling). Empty request bodies will also fail validation where a body is expected.

For basic validation, attributes can be added to a resource model, and it will be validated automatically before hitting an action method. For validation that requires more complex logic, simply throw an `InvalidResourceException` from anywhere within an action method, and a 400 response will be returned. Annotation based validation is achieved using [`ValidationFilter`](src/Bookatable.Migrations.MantleEventsMigrationApi/Filters/ValidationFilter.cs), and exception based validation is handled in the [`ErrorHandlingMiddleware`](src/Bookatable.Migrations.MantleEventsMigrationApi/Middleware/ErrorHandlingMiddleware.cs).

### Pagination

Pagination is header based, and uses same approach as [GitHub](https://developer.github.com/v3/guides/traversing-with-pagination/). Page links are returned in the `Link` header, and the total number of elements is returned in the `X-Total-Count` header. Collection endpoints are paginated with a page size of 10 (configurable) by default and query params can be used to override this.

```code
GET /foos?page=2&perPage=10
```

When an action method returns an instance of [`IPaginatedEnumerable<T>`](src/Bookatable.Migrations.MantleEventsMigrationApi/Model/Pagination/IPaginatedEnumerable.cs) it is picked up by the [`PaginationFilter`](src/Bookatable.Migrations.MantleEventsMigrationApi/Filters/PaginationFilter.cs) that constructs the `Link` header on the response.

In memory pagination is demonstrated in the solution, however in production applications pagination should ideally happen in downstream components e.g. databases. For more details on this, the dotnet core [documentation](https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/sort-filter-page) on the topic is a good reference.

### Documentation

[Swagger](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) is included in the API and will automatically document all endpoint. The Swagger UI is launched on startup, and can be used to browse the API and send test requests. `ProducesResponseType` attributes are required on action methods in order to include response descriptions in the documentation.

## TODO

- Add logging framework
- Attach a correlation ID to all requests
- Consider a 3rd part container like [Simple Injector](https://simpleinjector.org/index.html)
