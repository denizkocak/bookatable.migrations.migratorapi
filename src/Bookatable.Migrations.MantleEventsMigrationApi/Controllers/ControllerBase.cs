﻿using System.Net;
using Bookatable.Migrations.MantleEventsMigrationApi.Models.Contract;
using Microsoft.AspNetCore.Mvc;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Controllers
{
    [Route("[controller]")]
    [ProducesResponseType(typeof(Error), (int) HttpStatusCode.InternalServerError)]
    public class ControllerBase : Controller
    {
    }
}
