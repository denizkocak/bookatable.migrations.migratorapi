﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;
using Bookatable.Migrations.MantleEventsMigrationApi.Models.Contract;
using Bookatable.Migrations.MantleEventsMigrationApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Controllers
{
    public class MigrationsController : ControllerBase
    {
        private readonly IGenerateMantleCommandContainer _mantleCommandContainerGenerator;
        private readonly IRunMantleCommand _commandRunnerObject;
        private readonly IRunJobsOnBackground _backgroundTaskRunner;

        public MigrationsController(IGenerateMantleCommandContainer mantleCommandContainerGenerator, IRunMantleCommand commandRunnerObject, IRunJobsOnBackground backgroundTaskRunner)
        {
            _mantleCommandContainerGenerator = mantleCommandContainerGenerator;
            _commandRunnerObject = commandRunnerObject;
            _backgroundTaskRunner = backgroundTaskRunner;
        }

        [HttpPut("/mantle")]
        [ProducesResponseType(typeof(Error), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Post([FromBody] CreateMantleMigrationRequest request)
        {
            var queryContainer = GenerateQueryContainer(request);

            _backgroundTaskRunner.Run(queryContainer.Id.ToString(), () => _commandRunnerObject.RunAsync(queryContainer));

            return Created($"/migrations/{queryContainer.Id}", null);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Error), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Get(string id)
        {
            var status = _backgroundTaskRunner.GetStatus(id);

            if (status == null)
                return NotFound();

            return Ok(status.ToString());
        }

        private MantleCommandContainer GenerateQueryContainer(CreateMantleMigrationRequest request)
        {
            if(request.AggregateIds != null && request.AggregateIds.Any())
                _mantleCommandContainerGenerator.WithAggregateIds(request.AggregateIds);

            if(request.CorrelationIds != null && request.CorrelationIds.Any())
                _mantleCommandContainerGenerator.WithCorrelationIds(request.CorrelationIds.Select(Guid.Parse));

            if(request.EventTypes != null && request.EventTypes.Any())
                _mantleCommandContainerGenerator.WithEventTypes(request.EventTypes);

            if(request.StartingCommitSequence.HasValue)
                _mantleCommandContainerGenerator.StartingCommitSquence(request.StartingCommitSequence.Value);

            if(request.EndingCommitSequence.HasValue)
                _mantleCommandContainerGenerator.EndingCommitSquence(request.EndingCommitSequence.Value);

            _mantleCommandContainerGenerator.WithQueues(request.DomainEventsTargetQueue, request.WebDomainEventsTargetQueue);

            return _mantleCommandContainerGenerator.Build();
        }
    }
}
