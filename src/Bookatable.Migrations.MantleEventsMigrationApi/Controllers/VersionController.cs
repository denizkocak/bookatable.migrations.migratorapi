﻿using System.Net;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Controllers
{
    public class VersionController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(string), (int) HttpStatusCode.OK)]
        public IActionResult Get()
        {
            var version = Assembly.GetEntryAssembly()
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;

            return Ok(version);
        }
    }
}
