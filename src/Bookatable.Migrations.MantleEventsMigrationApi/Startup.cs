﻿using Bookatable.Migrations.MantleEventsMigrationApi.Filters;
using Bookatable.Migrations.MantleEventsMigrationApi.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Bookatable.Migrations.MantleEventsMigrationApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ValidationFilter));
            });

            if (Configuration.GetValue<bool>("Swagger:Enabled"))
            {
                services.AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new Info {Title = "bookatable migrations-mantleeventsmigrationapi", Version = "v1"});
                });
            }

            ContainerSetup.RegisterServices(services, Configuration);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<CorrelationIdMiddleware>()
                .UseMiddleware<ErrorHandlingMiddleware>();
            
            app.UseMvc();

            if (Configuration.GetValue<bool>("Swagger:Enabled"))
            {
                app.UseSwagger().UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "bookatable migrations-mantleeventsmigrationapi");
                }).UseRewriter(new RewriteOptions().AddRedirect("^$", "swagger"));
            }
        }
    }
}
