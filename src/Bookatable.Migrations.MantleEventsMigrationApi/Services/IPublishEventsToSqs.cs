﻿using System.Threading.Tasks;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public interface IPublishEventsToSqs
    {
        Task PublishAsync(string targetQueue, string messageBody);
    }
}
