﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public class MantleCommandContainerGenerator : IGenerateMantleCommandContainer
    {
        private readonly StringBuilder _selectQueryBuilder;
        private readonly StringBuilder _countQueryBuilder;
        private readonly List<string> _whereClauses;
        private readonly Dictionary<string, string> _parameterValues;
        private string _domainEventsTargetQueue;
        private string _webDomainEventsTargetQueue;

        public MantleCommandContainerGenerator()
        {
            _parameterValues = new Dictionary<string, string>();
            _whereClauses = new List<string>();
            _selectQueryBuilder = new StringBuilder();
            _countQueryBuilder = new StringBuilder();

            _selectQueryBuilder.Append("SELECT * FROM dbo.[EventJournal]");
            _countQueryBuilder.Append("SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal]");
        }

        public MantleCommandContainerGenerator WithAggregateIds(IEnumerable<string> aggregateIds)
        {
            var paramsString = SetParameterValues(aggregateIds.ToArray());

            _whereClauses.Add($"AggregateId in ({paramsString})");

            return this;
        }

        public MantleCommandContainerGenerator WithEventTypes(IEnumerable<string> eventTypes)
        {
            var paramsString = SetParameterValues(eventTypes.ToArray());

            _whereClauses.Add($"EventType in ({paramsString})");

            return this;
        }

        public MantleCommandContainerGenerator WithCorrelationIds(IEnumerable<Guid> correlationIds)
        {
            var paramsString = SetParameterValues(correlationIds.Select(x => x.ToString()).ToArray());

            _whereClauses.Add($"CorrelationId in ({paramsString})");

            return this;
        }

        public MantleCommandContainerGenerator StartingCommitSquence(int startingSequence)
        {
            var param = SetParameterValues(new[] {startingSequence.ToString()});

            _whereClauses.Add($"CommitSequence <= {param}");

            return this;
        }

        public MantleCommandContainerGenerator EndingCommitSquence(int endingSequence)
        {
            var param = SetParameterValues(new[] { endingSequence.ToString()});

            _whereClauses.Add($"CommitSequence >= {param}");

            return this;
        }

        public MantleCommandContainerGenerator WithQueues(string domainEventsTargetQueue, string webDomainEventsTargetQueue)
        {
            _domainEventsTargetQueue = domainEventsTargetQueue;
            _webDomainEventsTargetQueue = webDomainEventsTargetQueue;

            return this;
        }

        public MantleCommandContainer Build()
        {
            if (_whereClauses.Any())
            {
                _selectQueryBuilder.Append(" WHERE");
                _countQueryBuilder.Append(" WHERE");

                for (int i = 0; i < _whereClauses.Count; i++)
                {
                    if (i > 0)
                    {
                        _selectQueryBuilder.Append(" AND");
                        _countQueryBuilder.Append(" AND");
                    }

                    _selectQueryBuilder.Append($" {_whereClauses[i]}");
                    _countQueryBuilder.Append($" {_whereClauses[i]}");
                }
            }

            _selectQueryBuilder.Append($" ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY");

            var selectCommand = new SqlCommand(_selectQueryBuilder.ToString());
            var countQuery = new SqlCommand(_countQueryBuilder.ToString());

            foreach (var parameterValue in _parameterValues)
            {
                selectCommand.Parameters.AddWithValue(parameterValue.Key, parameterValue.Value);
                countQuery.Parameters.AddWithValue(parameterValue.Key, parameterValue.Value);
            }

            selectCommand.Parameters.AddWithValue("@skip", 0);
            selectCommand.Parameters.AddWithValue("@take", 100);

            return new MantleCommandContainer()
            {
                SelectQuery = selectCommand,
                CountQuery = countQuery,
                DomainEventsTargetQueue = _domainEventsTargetQueue,
                WebDomainEventsTargetQueue = _webDomainEventsTargetQueue,
                Id = Guid.NewGuid()
            };
        }

        private string SetParameterValues(string[] ids)
        {
            List<string> parameters = new List<string>();

            var currentParameterCount = _parameterValues.Count;
            for (int i = currentParameterCount; i < currentParameterCount + ids.Length; i++)
            {
                parameters.Add($"@p{i}");
                _parameterValues.Add($"@p{i}", ids[i - currentParameterCount]);
            }

            return string.Join(",", parameters);
        }
    }
}
