﻿using System;
using System.Threading.Tasks;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public interface IRunJobsOnBackground
    {
        void Run(string id, Func<Task> task);

        TaskStatus? GetStatus(string id);
    }
}
