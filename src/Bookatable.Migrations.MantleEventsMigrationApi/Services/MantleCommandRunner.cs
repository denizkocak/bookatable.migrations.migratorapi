﻿using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Configuration;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;
using Bookatable.Migrations.MantleEventsMigrationApi.Repositories;
using Mantle.Journal.Dto;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public class MantleCommandRunner : IRunMantleCommand
    {
        private readonly IMantleDbRepository _mantleDbRepository;
        private readonly IPublishEventsToSqs _eventPublisher;
        private readonly IOptions<MantleCommandRunnerConfig> _config;

        public MantleCommandRunner(IMantleDbRepository mantleDbRepository, IPublishEventsToSqs eventPublisher, IOptions<MantleCommandRunnerConfig> config)
        {
            _mantleDbRepository = mantleDbRepository;
            _eventPublisher = eventPublisher;
            _config = config;
        }

        public async Task RunAsync(MantleCommandContainer mantleCommandContainer)
        {
            var count = await _mantleDbRepository.GetCount(mantleCommandContainer.CountQuery);

            for (int i = 0; i < count; i = i + _config.Value.MantleQueryBatchSize)
            {
                var skip = 0;
                var take = _config.Value.MantleQueryBatchSize;

                while (count > 0)
                {
                    if (count < take)
                    {
                        take = count;
                    }

                    mantleCommandContainer.SelectQuery.Parameters["@skip"].Value = skip;
                    mantleCommandContainer.SelectQuery.Parameters["@take"].Value = take;

                    var journalEvents = await _mantleDbRepository.GetEvents(mantleCommandContainer.SelectQuery);

                    foreach(var journalEvent in journalEvents)
                    {
                        await PublishEventToQueue(mantleCommandContainer, journalEvent);
                    }

                    skip = skip + take;
                    count = count - take;
                }
            }
        }

        private async Task PublishEventToQueue(MantleCommandContainer mantleCommandContainer, JournalEvent journalEvent)
        {
            var queueName = journalEvent.EventType.StartsWith("Mantle.DomainEvents")
                ? mantleCommandContainer.DomainEventsTargetQueue
                : mantleCommandContainer.WebDomainEventsTargetQueue;

            await _eventPublisher.PublishAsync(queueName, JsonConvert.SerializeObject(journalEvent));
        }
    }
}
