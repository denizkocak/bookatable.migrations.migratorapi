﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public class BackgroundJobRunner : IRunJobsOnBackground
    {
        public Dictionary<string, Task> RunningTasks;

        public BackgroundJobRunner()
        {
            RunningTasks = new Dictionary<string, Task>();
        }

        public void Run(string id, Func<Task> task)
        {
            RunningTasks.Add(id, task());
        }

        public TaskStatus? GetStatus(string id)
        {
            return !RunningTasks.TryGetValue(id, out var task) ? null : task?.Status;
        }
    }
}
