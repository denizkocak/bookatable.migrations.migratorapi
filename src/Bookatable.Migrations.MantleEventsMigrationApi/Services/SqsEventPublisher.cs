﻿using System.Threading.Tasks;
using Amazon.SQS;
using Amazon.SQS.Model;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public class SqsEventPublisher : IPublishEventsToSqs
    {
        private readonly IAmazonSQS _sqsClient;

        public SqsEventPublisher(IAmazonSQS sqsClient)
        {
            _sqsClient = sqsClient;
        }

        public async Task PublishAsync(string targetQueue, string messageBody)
        {
            await _sqsClient.SendMessageAsync(new SendMessageRequest(targetQueue, messageBody));
        }
    }
}
