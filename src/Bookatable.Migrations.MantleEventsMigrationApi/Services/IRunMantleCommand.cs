﻿using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public interface IRunMantleCommand
    {
        Task RunAsync(MantleCommandContainer mantleCommandContainer);
    }
}