﻿using System;
using System.Collections.Generic;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Services
{
    public interface IGenerateMantleCommandContainer
    {
        MantleCommandContainerGenerator WithAggregateIds(IEnumerable<string> aggregateIds);

        MantleCommandContainerGenerator WithEventTypes(IEnumerable<string> eventTypes);

        MantleCommandContainerGenerator WithCorrelationIds(IEnumerable<Guid> correlationIds);

        MantleCommandContainerGenerator StartingCommitSquence(int startingSequence);

        MantleCommandContainerGenerator EndingCommitSquence(int endingSequence);

        MantleCommandContainerGenerator WithQueues(string domainEventsTargetQueue, string webDomainEventsTargetQueue);

        MantleCommandContainer Build();
    }
}
