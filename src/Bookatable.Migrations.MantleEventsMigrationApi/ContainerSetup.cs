﻿using Amazon.SQS;
using Bookatable.Migrations.MantleEventsMigrationApi.Configuration;
using Bookatable.Migrations.MantleEventsMigrationApi.Providers;
using Bookatable.Migrations.MantleEventsMigrationApi.Repositories;
using Bookatable.Migrations.MantleEventsMigrationApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bookatable.Migrations.MantleEventsMigrationApi
{
    public static class ContainerSetup
    {
        public static void RegisterServices(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.Configure<MantleCommandRunnerConfig>(options => configuration.GetSection("MantleCommandRunnerConfig").Bind(options));

            serviceCollection
                .AddLogging()
                .AddAWSService<IAmazonSQS>(configuration.GetAWSOptions("AWS:Sqs"))
                .AddSingleton<ICorrelationIdProvider, CorrelationIdProvider>()
                .AddTransient<IMantleDbRepository>(s => new MantleDbRepository(configuration.GetConnectionString("MantleConnection")))
                .AddTransient<IGenerateMantleCommandContainer, MantleCommandContainerGenerator>()
                .AddTransient<IPublishEventsToSqs, SqsEventPublisher>()
                .AddTransient<IRunMantleCommand, MantleCommandRunner>()
                .AddSingleton<IRunJobsOnBackground, BackgroundJobRunner>()
                .AddScoped<IHttpContextAccessor, HttpContextAccessor>();
        }
    }
}
