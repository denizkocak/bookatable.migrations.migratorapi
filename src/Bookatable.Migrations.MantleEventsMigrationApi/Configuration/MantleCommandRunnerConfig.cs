﻿namespace Bookatable.Migrations.MantleEventsMigrationApi.Configuration
{
    public class MantleCommandRunnerConfig
    {
        public int MantleQueryBatchSize { get; set; }
    }
}
