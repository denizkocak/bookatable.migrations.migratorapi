﻿using System;
using System.Net;
using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Models.Contract;
using Bookatable.Migrations.MantleEventsMigrationApi.Models.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IHostingEnvironment _env;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        private const string DefaultErrorMessage = "Oh no, something went wrong! Please consult application logs.";

        public ErrorHandlingMiddleware(RequestDelegate next, IHostingEnvironment env,
            ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _env = env;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An unhandled exception was thrown by the application.{Environment.NewLine}{ex}");
                await SetErrorResponseAsync(context, ex);
            }
        }

        private Task SetErrorResponseAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code;
            Error error;

            if (exception is InvalidResourceException invalidResourceException)
            {
                code = HttpStatusCode.BadRequest;
                error = new Error(invalidResourceException.Errors);
            }
            else
            {
                code = HttpStatusCode.InternalServerError;
                error = _env.IsDevelopment()
                    ? new Error(exception.ToString())
                    : new Error(DefaultErrorMessage);
            }

            var errorContent = JsonConvert.SerializeObject(error);
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.StatusCode = (int) code;
            return context.Response.WriteAsync(errorContent);
        }
    }
}
