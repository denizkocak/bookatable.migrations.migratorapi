﻿using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Middleware
{
    public class CorrelationIdMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ICorrelationIdProvider _correlationIdProvider;
        private readonly ILogger<CorrelationIdMiddleware> _logger;

        public CorrelationIdMiddleware(RequestDelegate next, ICorrelationIdProvider correlationIdProvider,
            ILogger<CorrelationIdMiddleware> logger)
        {
            _next = next;
            _correlationIdProvider = correlationIdProvider;
            _logger = logger;
        }

        public Task Invoke(HttpContext context)
        {
            using (_logger.BeginScope(("correlation_id", _correlationIdProvider.CorrelationId)))
            {
                return _next.Invoke(context);
            }
        }
    }
}
