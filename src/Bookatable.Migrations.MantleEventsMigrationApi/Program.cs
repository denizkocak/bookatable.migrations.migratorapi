﻿using System;
using System.IO;
using Gelf.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Bookatable.Migrations.MantleEventsMigrationApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration(ConfigureAppConfiguration)
                .ConfigureLogging(ConfigureLogging)
                .Build();

            host.Run();
        }

        private static void ConfigureAppConfiguration(WebHostBuilderContext context, IConfigurationBuilder builder)
        {
            var env = context.HostingEnvironment;

            builder.SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }

        private static void ConfigureLogging(WebHostBuilderContext context, ILoggingBuilder builder)
        {
            var configuration = context.Configuration;

            var graylogConfig = configuration.GetSection("Graylog").Get<GelfLoggerOptions>();
            graylogConfig.LogSource = Environment.MachineName;

            builder.AddConfiguration(configuration.GetSection("Logging"))
                .AddConsole()
                .AddDebug()
                .AddProvider(new GelfLoggerProvider(graylogConfig));
        }
    }
}
