﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Mantle.Journal.Dto;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Repositories
{
    public interface IMantleDbRepository
    {
        Task<int> GetCount(SqlCommand countQuery);

        Task<IEnumerable<JournalEvent>> GetEvents(SqlCommand getEventCommand);
    }
}
