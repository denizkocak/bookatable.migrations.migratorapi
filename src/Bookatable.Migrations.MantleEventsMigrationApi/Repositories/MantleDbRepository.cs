﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Mantle.Journal.Dto;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Repositories
{
    public class MantleDbRepository : IMantleDbRepository
    {
        private readonly string _mantleConnectionString;

        public MantleDbRepository(string mantleConnectionString)
        {
            _mantleConnectionString = mantleConnectionString;
        }

        public async Task<int> GetCount(SqlCommand countQuery)
        {
            var totalCount = 0;
            using (var connection = new SqlConnection(_mantleConnectionString))
            {
                countQuery.Connection = connection;
                using (var command = countQuery)
                {
                    command.CommandType = CommandType.Text;

                    await connection.OpenAsync();
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            totalCount = (int)reader["TotalCount"];
                        }
                    }
                }
            }

            return totalCount;
        }

        public async Task<IEnumerable<JournalEvent>> GetEvents(SqlCommand getEventCommand)
        {
            using (var connection = new SqlConnection(_mantleConnectionString))
            {
                getEventCommand.Connection = connection;
                using (var command = getEventCommand)
                {
                    command.CommandType = CommandType.Text;

                    await connection.OpenAsync();
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return await ParseResult(reader);
                    }
                }
            }
        }

        private async Task<List<JournalEvent>> ParseResult(SqlDataReader reader)
        {
            var result = new List<JournalEvent>();
            while (await reader.ReadAsync())
            {
                var item = new JournalEvent()
                {
                    Id = (Guid)reader["Id"],
                    AggregateId = reader["AggregateId"].ToString(),
                    AggregateType = reader["AggregateType"].ToString(),
                    CorrelationId = (Guid)reader["CorrelationId"],
                    DateLogged = (DateTime)reader["DateLogged"],
                    EventBody = reader["EventBody"].ToString(),
                    EventType = reader["EventType"].ToString(),
                    Version = (int)reader["StreamVersion"],
                    CommitSequence = (int)reader["CommitSequence"]
                };
                result.Add(item);
            }

            return result;
        }
    }
}
