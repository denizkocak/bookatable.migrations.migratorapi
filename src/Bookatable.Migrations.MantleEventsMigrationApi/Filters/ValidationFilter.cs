﻿using System.Linq;
using System.Text.RegularExpressions;
using Bookatable.Migrations.MantleEventsMigrationApi.Models.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Filters
{
    public class ValidationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ActionArguments.Any(a => a.Value == null))
            {
                context.Result = new BadRequestObjectResult(new Error("Required data is missing."));
                return;
            }

            if (context.ModelState.IsValid)
            {
                return;
            }

            var valdiationErrors = context.ModelState.Where(kvp => !string.IsNullOrEmpty(kvp.Key));
            var validationErrorMessages = valdiationErrors.SelectMany(kvp =>
                kvp.Value.Errors.Select(e => GetModelStateErrorMessage(kvp.Key, e))).ToArray();

            context.Result = new BadRequestObjectResult(validationErrorMessages.Any()
                ? new Error(validationErrorMessages)
                : new Error("Invalid request."));
        }

        private static string GetModelStateErrorMessage(string key, ModelError error)
        {
            var message = error.Exception?.Message ?? error.ErrorMessage;

            var camelCaseKey = $"{key.Substring(0, 1).ToLowerInvariant()}{key.Substring(1)}";
            return Regex.Replace(message, $@"\b{Regex.Escape(key)}\b", camelCaseKey);
        }
    }
}
