using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Providers
{
    public class CorrelationIdProvider : ICorrelationIdProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _correlationId;

        public CorrelationIdProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string CorrelationId => _correlationId ?? (_correlationId = GetCorrelationId());

        private string GetCorrelationId()
        {
            var context = _httpContextAccessor.HttpContext;

            return context.Request.Headers.TryGetValue("X-Correlation-ID", out StringValues correlationId)
                ? (string) correlationId
                : Guid.NewGuid().ToString();
        }
    }
}
