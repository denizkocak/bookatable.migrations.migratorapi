﻿namespace Bookatable.Migrations.MantleEventsMigrationApi.Providers
{
    public interface ICorrelationIdProvider
    {
        string CorrelationId { get; }
    }
}
