﻿using System;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Models.Exceptions
{
    public class InvalidResourceException : Exception
    {
        public InvalidResourceException(string message) : base(message)
        {
        }

        public InvalidResourceException(string message, string[] errors) : base(message)
        {
            Errors = errors;
        }

        public string[] Errors { get; set; }
    }
}
