﻿namespace Bookatable.Migrations.MantleEventsMigrationApi.Models.Contract
{
    public class ErrorDetail
    {
        public ErrorDetail(string message)
        {
            Message = message;
        }

        public string Code { get; set; }
        public string Message { get; set; }
    }
}
