﻿using System.Collections.Generic;
using System.Linq;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Models.Contract
{
    public class Error
    {
        public Error(string message)
        {
            Errors = new[] {new ErrorDetail(message)};
        }

        public Error(IEnumerable<string> messages)
        {
            Errors = messages.Select(m => new ErrorDetail(m)).ToArray();
        }

        public ErrorDetail[] Errors { get; set; }
    }
}
