﻿using System;
using System.Data.SqlClient;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Models
{
    public class MantleCommandContainer
    {
        public string DomainEventsTargetQueue { get; set; }

        public string WebDomainEventsTargetQueue { get; set; }

        public SqlCommand SelectQuery { get; set; }

        public SqlCommand CountQuery { get; set; }

        public Guid Id { get; set; }
    }
}
