﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bookatable.Migrations.MantleEventsMigrationApi.Models
{
    public class CreateMantleMigrationRequest
    {
        [Required]
        public string DomainEventsTargetQueue { get; set; }

        [Required]
        public string WebDomainEventsTargetQueue { get; set; }

        public IEnumerable<string> AggregateIds { get; set; }

        public IEnumerable<string> EventTypes { get; set; }

        public IEnumerable<string> CorrelationIds { get; set; }

        public int? StartingCommitSequence { get; set; }

        public int? EndingCommitSequence { get; set; }
    }
}
