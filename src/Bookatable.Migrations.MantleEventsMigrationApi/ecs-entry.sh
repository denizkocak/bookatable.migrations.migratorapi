#!/bin/bash
export DATADOG__STATSDSERVERNAME=$(curl -s 169.254.169.254/latest/meta-data/local-ipv4)
exec "$@"
