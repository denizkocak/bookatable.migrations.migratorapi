﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bookatable.Migrations.MantleEventsMigrationApi.IntegrationTests
{
    public static class TestHelpers
    {
        public static TestServer CreateTestServer()
        {
            var config = new ConfigurationBuilder().Build();

            var host = new WebHostBuilder()
                .UseConfiguration(config)
                .UseStartup<Startup>();

            return new TestServer(host);
        }

        public static TestServer CreateTestServer(Action<IServiceCollection> configureServices)
        {
            var config = new ConfigurationBuilder().Build();

            var host = new WebHostBuilder()
                .UseConfiguration(config)
                .UseStartup<Startup>()
                .ConfigureServices(configureServices);

            return new TestServer(host);
        }
    }
}
