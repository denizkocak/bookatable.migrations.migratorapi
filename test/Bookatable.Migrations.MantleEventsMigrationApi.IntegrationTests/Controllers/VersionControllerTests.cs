﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace Bookatable.Migrations.MantleEventsMigrationApi.IntegrationTests.Controllers
{
    public class VersionControllerTests
    {
        private readonly TestServer _testServer;

        public VersionControllerTests()
        {
            _testServer = TestHelpers.CreateTestServer();
        }

        [Fact]
        public async Task Get_Returns200StatusCode()
        {
            using (var client = _testServer.CreateClient())
            {
                var requestMessage = new HttpRequestMessage(HttpMethod.Get, "/version");
                var responseMessage = await client.SendAsync(requestMessage);

                Assert.Equal(HttpStatusCode.OK, responseMessage.StatusCode);
            }
        }
    }
}
