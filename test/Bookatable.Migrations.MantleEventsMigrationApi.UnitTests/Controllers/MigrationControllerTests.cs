﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Controllers;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;
using Bookatable.Migrations.MantleEventsMigrationApi.Services;
using Moq;
using Xunit;

namespace Bookatable.Migrations.MantleEventsMigrationApi.UnitTests.Controllers
{
    public class MigrationControllerTests
    {
        private MigrationsController _target;
        private readonly Mock<IGenerateMantleCommandContainer> _queryGenerator;
        private readonly Mock<IRunJobsOnBackground> _backgroundTaskRunner;
        private Mock<IRunMantleCommand> _commandRunner;

        public MigrationControllerTests()
        {
            _commandRunner = new Mock<IRunMantleCommand>();
            _queryGenerator = new Mock<IGenerateMantleCommandContainer>();
            _backgroundTaskRunner = new Mock<IRunJobsOnBackground>();

            var commandContainer = new MantleCommandContainer();
            _queryGenerator.Setup(x => x.Build()).Returns(commandContainer);

            _target = new MigrationsController(_queryGenerator.Object, _commandRunner.Object, _backgroundTaskRunner.Object);
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequestThatHasAggregateIds_CallsQueryGenerator()
        {
            var ids = new[] { "1", "2" };
            var request = new CreateMantleMigrationRequest(){AggregateIds = ids};

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.WithAggregateIds(ids));
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequestThatHasCorrelationIds_CallsQueryGenerator()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var ids = new[] { id1.ToString(), id2.ToString() };
            var request = new CreateMantleMigrationRequest(){CorrelationIds = ids};

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.WithCorrelationIds(It.Is<IEnumerable<Guid>>(g => g.Contains(id1) && g.Contains(id2))));
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequestThatHasEventTypes_CallsQueryGenerator()
        {
            var eventTypes = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
            var request = new CreateMantleMigrationRequest(){EventTypes = eventTypes };

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.WithEventTypes(eventTypes));
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequestThatHasStartingCommitSequence_CallsQueryGenerator()
        {
            var request = new CreateMantleMigrationRequest(){StartingCommitSequence = 1 };

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.StartingCommitSquence(1));
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequestThatHasEndingCommitSequence_CallsQueryGenerator()
        {
            var request = new CreateMantleMigrationRequest(){EndingCommitSequence = 1 };

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.EndingCommitSquence(1));
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequestThatHasTargetQeueus_CallsQueryGenerator()
        {
            var domainEventsTargetQueue = "http://queueUrl";
            var webDomainEventsTargetQueue = "http://queueUrl2";
            var request = new CreateMantleMigrationRequest(){WebDomainEventsTargetQueue = webDomainEventsTargetQueue, DomainEventsTargetQueue = domainEventsTargetQueue};

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.WithQueues(domainEventsTargetQueue, webDomainEventsTargetQueue));
        }

        [Fact]
        public async Task Post_WhenCalledWithCreateMigrationRequest_BuildsQuery()
        {
            var request = new CreateMantleMigrationRequest(){AggregateIds = new []{"1"} };

            await _target.Post(request);

            _queryGenerator.Verify(x =>x.Build());
        }

        [Fact]
        public async Task Post_WhenCommandIsCreated_CallsCommandRunner()
        {
            var domainEventsTargetQueue = "http://queueUrl";
            var webDomainEventsTargetQueue = "http://queueUrl2";
            var request = new CreateMantleMigrationRequest()
            {
                WebDomainEventsTargetQueue = webDomainEventsTargetQueue,
                DomainEventsTargetQueue = domainEventsTargetQueue
            };

            var commandContainer = new MantleCommandContainer();
            _queryGenerator.Setup(x => x.Build()).Returns(commandContainer);

            await _target.Post(request);
            
            _backgroundTaskRunner.Verify(x => x.Run(It.IsAny<string>(), It.IsAny<Func<Task>>()));
        }
    }
}
