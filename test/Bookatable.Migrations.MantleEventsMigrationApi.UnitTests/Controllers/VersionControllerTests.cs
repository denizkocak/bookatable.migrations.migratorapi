﻿using System.Reflection;
using Bookatable.Migrations.MantleEventsMigrationApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Bookatable.Migrations.MantleEventsMigrationApi.UnitTests.Controllers
{
    public class VersionControllerTests
    {
        private readonly VersionController _versionController;

        public VersionControllerTests()
        {
            _versionController = new VersionController();
        }

        [Fact]
        public void GetAsync_ReturnsTheCorrectVersion()
        {
            var expectedVersion = Assembly.GetEntryAssembly()
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;

            var actual = (OkObjectResult) _versionController.Get();
            Assert.Equal(expectedVersion, actual.Value);
        }

        [Fact]
        public void GetAsync_Returns200StatusCode()
        {
            var actual = (OkObjectResult) _versionController.Get();
            Assert.Equal(200, actual.StatusCode);
        }
    }
}
