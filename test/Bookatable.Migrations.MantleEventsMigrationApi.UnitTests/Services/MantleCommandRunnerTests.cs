﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Bookatable.Migrations.MantleEventsMigrationApi.Configuration;
using Bookatable.Migrations.MantleEventsMigrationApi.Models;
using Bookatable.Migrations.MantleEventsMigrationApi.Repositories;
using Bookatable.Migrations.MantleEventsMigrationApi.Services;
using Mantle.Journal.Dto;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace Bookatable.Migrations.MantleEventsMigrationApi.UnitTests.Services
{
    public class MantleCommandRunnerTests
    {
        private MantleCommandRunner _target;
        private readonly Mock<IMantleDbRepository> _mantleDbRepository;
        private int _batchSize = 1;
        private IOptions<MantleCommandRunnerConfig> _config;
        private SqlCommand _countQuery;
        private SqlCommand _selectQuery;
        private MantleCommandContainer _mantleCommandContainer;
        private Mock<IPublishEventsToSqs> _eventPublisher;
        private string _domainEventsTargetQueue;
        private string _webDomainEventsTargetQueue;

        public MantleCommandRunnerTests()
        {
            _countQuery = new SqlCommand("SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where AggregateId in (@p0,@p1)");
            _selectQuery = new SqlCommand("SELECT * FROM dbo.[EventJournal] Where AggregateId in (@p0,@p1) OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY");

            _selectQuery.Parameters.AddWithValue("@skip", 0);
            _selectQuery.Parameters.AddWithValue("@take", 100);
            _domainEventsTargetQueue = "http://sqs.domaineventsUrl";
            _webDomainEventsTargetQueue = "http://sqs.webdomaineventsUrl";

            _mantleCommandContainer = new MantleCommandContainer()
            {
                DomainEventsTargetQueue = _domainEventsTargetQueue,
                WebDomainEventsTargetQueue = _webDomainEventsTargetQueue,
                CountQuery = _countQuery,
                SelectQuery = _selectQuery
            };

            _config = Options.Create(new MantleCommandRunnerConfig
            {
                MantleQueryBatchSize = _batchSize
            });

            _mantleDbRepository = new Mock<IMantleDbRepository>();
            _eventPublisher = new Mock<IPublishEventsToSqs>();

            _target = new MantleCommandRunner(_mantleDbRepository.Object, _eventPublisher.Object, _config);
        }

        [Fact]
        public async Task RunAsync_WhenCalledWithMantleQueryContainer_RequestEventsFromRepositoryWithGivenQueries()
        {
            _config.Value.MantleQueryBatchSize = 1;
            _target = new MantleCommandRunner(_mantleDbRepository.Object, _eventPublisher.Object, _config);

            _mantleDbRepository.Setup(x => x.GetCount(_countQuery)).ReturnsAsync(5);

            await _target.RunAsync( _mantleCommandContainer);

            _mantleDbRepository.Verify(x => x.GetEvents(It.IsAny<SqlCommand>()), Times.Exactly(5));
        }

        [Fact]
        public async Task RunAsync_WhenCalledWithMantleQueryContainer_ShouldSetCommandParameterForEachBatch()
        {
            _config.Value.MantleQueryBatchSize = 1;
            _target = new MantleCommandRunner(_mantleDbRepository.Object, _eventPublisher.Object, _config);

            _mantleDbRepository.Setup(x => x.GetCount(_countQuery)).ReturnsAsync(1);

            await _target.RunAsync( _mantleCommandContainer);

            _mantleDbRepository.Verify(x => x.GetEvents(It.Is<SqlCommand>(c => c.Parameters["@skip"].Value.ToString() == "0" && c.Parameters["@take"].Value.ToString() == "1")));
        }

        [Fact]
        public async Task RunAsync_WhenJournalEventTypeIsDomainEvent_PublishesEventsToTheDomainEventQueue()
        {
            _mantleDbRepository.Setup(x => x.GetCount(_countQuery)).ReturnsAsync(1);
            var journalEvent = new JournalEvent()
            {
                EventType = "Mantle.DomainEvents.Events.RestaurantAccountUpdated",
                AggregateId = "123"
            };
            _mantleDbRepository.Setup(x => x.GetEvents(It.IsAny<SqlCommand>())).ReturnsAsync(new []{journalEvent});

            await _target.RunAsync(_mantleCommandContainer);

            _eventPublisher.Verify(x => x.PublishAsync(_domainEventsTargetQueue, JsonConvert.SerializeObject(journalEvent)));
        }

        [Fact]
        public async Task RunAsync_WhenJournalEventTypeIsWebDomainEvent_PublishesEventsToTheWebDomainEventQueue()
        {
            _mantleDbRepository.Setup(x => x.GetCount(_countQuery)).ReturnsAsync(1);
            var journalEvent = new JournalEvent()
            {
                EventType = "Mantle.WebDomainEvents.Events.RestaurantAccountUpdated",
                AggregateId = "123"
            };
            _mantleDbRepository.Setup(x => x.GetEvents(It.IsAny<SqlCommand>())).ReturnsAsync(new []{journalEvent});

            await _target.RunAsync(_mantleCommandContainer);

            _eventPublisher.Verify(x => x.PublishAsync(_webDomainEventsTargetQueue, JsonConvert.SerializeObject(journalEvent)));
        }
    }
}
