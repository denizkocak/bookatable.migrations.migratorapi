﻿using System;
using Bookatable.Migrations.MantleEventsMigrationApi.Services;
using Xunit;

namespace Bookatable.Migrations.MantleEventsMigrationApi.UnitTests.Services
{
    public class MantleCommandGeneratorTests
    {
        private MantleCommandContainerGenerator _target;

        public MantleCommandGeneratorTests()
        {
            _target = new MantleCommandContainerGenerator();
        }

        [Fact]
        public void Build_WhenCalledWithoutAnyFilter_ReturnsQueryWithCorrectQueries()
        {
            var result = _target.Build();

            string selectQuery = "SELECT * FROM dbo.[EventJournal] ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            string countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal]";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithAggregateIds_ReturnsQueryWithCorrectFilter()
        {
            var aggregateIds = new []{"1","2"};

            var result = _target.WithAggregateIds(aggregateIds).Build();

            string selectQuery = "SELECT * FROM dbo.[EventJournal] Where AggregateId in (@p0,@p1) ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            string countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where AggregateId in (@p0,@p1)";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithAggregateIdsAndEventTypes_ReturnsQueryWithCorrectFilter()
        {
            var aggregateIds = new []{"1","2"};
            var eventTypes = new []{ "Mantle.DomainEvents.Events.RestaurantAccountUpdated", "Mantle.DomainEvents.Events.AddedNetworkDescriptionToRestaurant" };

            var result = _target.WithAggregateIds(aggregateIds).WithEventTypes(eventTypes).Build();

            var selectQuery = "SELECT * FROM dbo.[EventJournal] Where AggregateId in (@p0,@p1) AND EventType in (@p2,@p3) ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            var countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where AggregateId in (@p0,@p1) AND EventType in (@p2,@p3)";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithCorrelationIdsAndEventTypes_ReturnsQueryWithCorrectFilter()
        {
            var correlationIds = new[] { Guid.NewGuid(), Guid.NewGuid() };
            var eventTypes = new[] { "Mantle.DomainEvents.Events.RestaurantAccountUpdated", "Mantle.DomainEvents.Events.AddedNetworkDescriptionToRestaurant" };

            var result = _target.WithCorrelationIds(correlationIds).WithEventTypes(eventTypes).Build();

            var selectQuery = "SELECT * FROM dbo.[EventJournal] Where CorrelationId in (@p0,@p1) AND EventType in (@p2,@p3) ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            var countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where CorrelationId in (@p0,@p1) AND EventType in (@p2,@p3)";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithAggregateIdsAndCorrelationIdsAndEventTypes_ReturnsQueryWithCorrectFilter()
        {
            var aggregateIds = new []{"1","2"};
            var correlationIds = new[] { Guid.NewGuid(), Guid.NewGuid() };
            var eventTypes = new[] { "Mantle.DomainEvents.Events.RestaurantAccountUpdated", "Mantle.DomainEvents.Events.AddedNetworkDescriptionToRestaurant" };

            var result = _target.WithAggregateIds(aggregateIds).WithCorrelationIds(correlationIds).WithEventTypes(eventTypes).Build();

            var selectQuery = "SELECT * FROM dbo.[EventJournal] Where AggregateId in (@p0,@p1) and CorrelationId in (@p2,@p3) AND EventType in (@p4,@p5) ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            var countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where AggregateId in (@p0,@p1) and CorrelationId in (@p2,@p3) AND EventType in (@p4,@p5)";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithAggregateIdsAndStartingCommitSequence_ReturnsQueryWithCorrectFilter()
        {
            var aggregateIds = new[] { "1", "2" };

            int startingSequence = 2;
            var result = _target.WithAggregateIds(aggregateIds).StartingCommitSquence(startingSequence).Build();

            var selectQuery = "SELECT * FROM dbo.[EventJournal] Where AggregateId in (@p0,@p1) AND CommitSequence <= @p2 ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            var countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where AggregateId in (@p0,@p1) AND CommitSequence <= @p2";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithAggregateIdsAndEndingCommitSequence_ReturnsQueryWithCorrectFilter()
        {
            var aggregateIds = new[] { "1", "2" };

            int endingSequence = 2;
            var result = _target.WithAggregateIds(aggregateIds).EndingCommitSquence(endingSequence).Build();

            var selectQuery = "SELECT * FROM dbo.[EventJournal] Where AggregateId in (@p0,@p1) AND CommitSequence >= @p2 ORDER BY [CommitSequence] OFFSET (@skip) ROWS FETCH NEXT (@take) ROWS ONLY";
            var countQuery = "SELECT COUNT(*) AS TotalCount FROM [dbo].[EventJournal] Where AggregateId in (@p0,@p1) AND CommitSequence >= @p2";

            Assert.Equal(selectQuery.ToLower(), result.SelectQuery.CommandText.ToLower());
            Assert.Equal(countQuery.ToLower(), result.CountQuery.CommandText.ToLower());
        }

        [Fact]
        public void Build_WhenCalledWithDomainEventsTargetQueueUrl_UpdatesTheCommandWithValue()
        {
            var domainEventsTargetQueue = "http://queueUrl";
            var webDomainEventsTargetQueue = "http://queueUrl2";

            _target.WithQueues(domainEventsTargetQueue, webDomainEventsTargetQueue).Build();
        }
    }
}
